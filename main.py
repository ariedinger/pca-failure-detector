from PIL import Image as img
import numpy as np
from sklearn.decomposition import PCA
import os
import sys
import cv2
import warnings

def readImage(filePath):
    try:
        image = img.open(filePath)
        return image
    except IOError:
        print(f"Unable to open image file: {filePath}")
        return None

def segmentBlades(image):
    # Convert the image to the OpenCV format (BGR)
    opencvImage = cv2.cvtColor(np.array(image), cv2.COLOR_RGBA2BGR)

    # Define the color range for the blades (you might need to adjust these values)
    lower_bound = np.array([0, 0, 0], dtype=np.uint8)
    upper_bound = np.array([100, 100, 100], dtype=np.uint8)

    # Create a binary mask for the blades
    mask = cv2.inRange(opencvImage, lower_bound, upper_bound)

    # Apply the binary mask to the original image
    resultImage = img.fromarray(cv2.bitwise_and(opencvImage, opencvImage, mask=mask))

    return resultImage

def dynamicThresholding(image):
    # Convert the image to grayscale
    grayscaleImage = cv2.cvtColor(np.array(image), cv2.COLOR_RGBA2GRAY)

    # Use adaptive thresholding
    _, thresholdedImage = cv2.threshold(grayscaleImage, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)

    return img.fromarray(thresholdedImage)

def flattenImage(image):
    return np.array(image).flatten()

def detectImageDifference(image1, image2):
    # Convert images to grayscale and apply dynamic thresholding
    thresholdedImage1 = dynamicThresholding(segmentBlades(image1))
    thresholdedImage2 = dynamicThresholding(segmentBlades(image2))

    # Flatten the thresholded images
    flattenedImage1 = flattenImage(thresholdedImage1)
    flattenedImage2 = flattenImage(thresholdedImage2)

    # Combine the flattened images into a single matrix
    combinedMatrix = np.vstack((flattenedImage1, flattenedImage2))

    # Apply PCA
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")  # Suppress the warnings
        pca = PCA()
        principalComponents = pca.fit_transform(combinedMatrix)

    # Compare the principal components of both images
    # Check if the variation in the principal components is significant
    if np.var(principalComponents, axis=0)[0] > 0.01:
        return True  # Significant difference
    else:
        return False  # No significant difference

if __name__ == "__main__":
    if len(sys.argv) < 3:
        print("Please provide a folder path and a base image filename as arguments.")
        sys.exit(1)

    folderPath = sys.argv[1]
    baseImagePath = sys.argv[2]

    baseImage = readImage(baseImagePath)

    if baseImage is None:
        print("Unable to read the base image.")
        sys.exit(1)

    imageFiles = sorted([file for file in os.listdir(folderPath) if file.endswith(".png") or file.endswith(".jpg")])
    print("Reading images list:")
    print(imageFiles)

    if len(imageFiles) < 2:
        print("Please provide at least two image files in the folder.")
        sys.exit(1)

    for i in range(0, len(imageFiles)):  # Start from the second image
        imagePath = os.path.join(folderPath, imageFiles[i])
        currentImage = readImage(imagePath)

        if currentImage is not None:
            with warnings.catch_warnings():
                warnings.simplefilter("ignore")  # Suppress the warnings
                result = detectImageDifference(baseImage, currentImage)
            print(f"File {imageFiles[i]}: {'failure' if result else 'pass'}.")
